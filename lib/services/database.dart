import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_test_app/models/brew.dart';
import 'package:flutter_test_app/models/user.dart';

class DatabaseService {
  // collection reference

  final CollectionReference brewCollection = Firestore.instance.collection("brews");
  final String uid;

  DatabaseService({this.uid});

  Future updateUserData(String sugar, String name, int strength) async {
    await brewCollection.document(uid).setData({'sugar': sugar, 'name': name, "strength": strength});
  }

  //brew list from snapshot
  List<Brew> _brewListFromSnapshot(QuerySnapshot snapshots) {
    return snapshots.documents.map((doc) {
      return Brew(name: doc.data['name'] ?? '', strength: doc.data['strength'] ?? 0, sugars: doc.data['sugar'] ?? '0');
    }).toList();
  }

  UserData _userDataFromSnapshot(DocumentSnapshot snapshots) {
    return UserData(
      uid: uid,
      name: snapshots['name'],
      sugars: snapshots['sugars'],
      strength: snapshots['strength'],
    );
  }

  Stream<List<Brew>> get brews {
    return brewCollection.snapshots().map(_brewListFromSnapshot);
  }

  Stream<UserData> get userData {
    return brewCollection.document(uid).snapshots().map(_userDataFromSnapshot);
  }
}
