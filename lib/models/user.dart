class User {
  final String uid;

  User({this.uid});

  String toString() {
    return '{ uid=' + this.uid + " }";
  }
}

class UserData {
  final String uid;
  final String name;
  final String sugars;
  final int strength;

  UserData({this.uid, this.name, this.sugars, this.strength});

  String toString() {
    return '{ uid=' + this.uid + " }" + '{ name=' + this.name + " }" + '{ sugars=' + this.sugars + " }" + '{ strength=' + this.strength.toString() + " }";
  }
}
